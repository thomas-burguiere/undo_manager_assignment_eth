package undo;

import org.junit.Before;
import org.junit.Test;
import undo.change.Change;
import undo.change.ChangeFactory;
import undo.change.ChangeFactoryImpl;
import undo.document.Document;
import undo.document.DocumentImpl;

import static org.assertj.core.api.Assertions.assertThat;

public class IntegrationTest {

    private static final int BUFFER_SIZE = 5;
    private Document document;
    private UndoManager undoManager;
    private ChangeFactory changeFactory;

    @Before
    public void beforeEach() {
        document = new DocumentImpl();
        undoManager = new UndoManagerFactoryImpl().createUndoManager(document, BUFFER_SIZE);
        changeFactory = new ChangeFactoryImpl();
    }

    @Test
    public void should_undo_and_redo_insertion() {
        // given
        final Change initialInsertion = changeFactory.createInsertion(0, "aaaa", 0, 4);
        initialInsertion.apply(document);

        // when
        final Change middleInsertion = changeFactory.createInsertion(2, "b", 2, 3);
        middleInsertion.apply(document);
        undoManager.registerChange(middleInsertion);

        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("aabaa");


        // when
        undoManager.undo();
        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("aaaa");

        // when
        undoManager.redo();
        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("aabaa");

    }



    @Test
    public void should_undo_and_redo_several_insertion_and_deletions() {
        // given
        final Change initialInsertion = changeFactory.createInsertion(0, "aaaa", 0, 4);
        initialInsertion.apply(document);

        // when
        final Change insertion1 = changeFactory.createInsertion(2, "b", 2, 3);
        insertion1.apply(document);
        undoManager.registerChange(insertion1);

        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("aabaa");


        // when
        final Change insertion2 = changeFactory.createInsertion(3, "c", 3, 4);
        insertion2.apply(document);
        undoManager.registerChange(insertion2);

        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("aabcaa");


        // when
        final Change deletion3 = changeFactory.createDeletion(0, "aa", 0, 0);
        deletion3.apply(document);
        undoManager.registerChange(deletion3);

        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("bcaa");

        // when
        undoManager.undo();
        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("aabcaa");

        // when
        undoManager.undo();
        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("aabaa");


        // when
        undoManager.redo();
        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("aabcaa");

        // when
        undoManager.redo();
        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("bcaa");

    }


}
