package undo;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import undo.change.Change;
import undo.change.Deletion;
import undo.change.Insertion;
import undo.document.Document;
import undo.document.DocumentImpl;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

public class UndoManagerImplTest {

    private static final int BUFFER_SIZE = 3;
    private UndoManager undoManager;
    private final DocumentImpl document = new DocumentImpl();


    @Before
    public void beforeEach() {
        undoManager = new UndoManagerImpl(document, BUFFER_SIZE);
    }

    @Test
    public void should_be_able_to_add_change() {
        //given

        final Change deletion1 = new Deletion();
        final Change insertion1 = new Insertion();
        final Change deletion2 = new Deletion();

        // when
        undoManager.registerChange(deletion1);
        undoManager.registerChange(insertion1);
        undoManager.registerChange(deletion2);

        // then
        final List<Change> actualUndoableChangeList = ((UndoManagerImpl) undoManager).getUndoableChangeList();
        assertThat(actualUndoableChangeList.get(0)).isSameAs(deletion1);
        assertThat(actualUndoableChangeList.get(1)).isSameAs(insertion1);
        assertThat(actualUndoableChangeList.get(2)).isSameAs(deletion2);
    }

    @Test
    public void should_add_change_and_remove_oldest_when_buffer_size_limit_is_reached() {
        //given

        final Change insertion1 = new Insertion();
        final Change insertion2 = new Insertion();
        final Change insertion3 = new Insertion();

        undoManager.registerChange(insertion1);
        undoManager.registerChange(insertion2);
        undoManager.registerChange(insertion3);

        final Change deletion4 = new Deletion();

        // when
        undoManager.registerChange(deletion4);


        // then
        final List<Change> actualUndoableChangeList = ((UndoManagerImpl) undoManager).getUndoableChangeList();
        assertThat(actualUndoableChangeList).hasSize(3);
        assertThat(actualUndoableChangeList.get(0)).isSameAs(insertion2);
        assertThat(actualUndoableChangeList.get(1)).isSameAs(insertion3);
        assertThat(actualUndoableChangeList.get(2)).isSameAs(deletion4);
    }

    @Test
    public void should_be_able_to_undo_and_redo() {
        //given

        final Change insertion1 = new Insertion();
        final Change insertion2 = new Insertion();
        final Change insertion3 = new Insertion();

        undoManager.registerChange(insertion1);
        undoManager.registerChange(insertion2);
        undoManager.registerChange(insertion3);

        final List<Change> undoableChangeList = ((UndoManagerImpl) undoManager).getUndoableChangeList();
        final List<Change> redoableChangeList = ((UndoManagerImpl) undoManager).getRedoableChangeList();

        // then
        assertThat(undoManager.canUndo()).isTrue();
        assertThat(undoManager.canRedo()).isFalse();

        // when
        undoManager.undo();
        // then

        assertThat(undoManager.canRedo()).isTrue();
        assertThat(redoableChangeList).hasSize(1);
        assertThat(redoableChangeList.get(0)).isSameAs(insertion3);

        assertThat(undoManager.canUndo()).isTrue();
        assertThat(undoableChangeList).hasSize(2);
        assertThat(undoableChangeList.get(0)).isSameAs(insertion1);
        assertThat(undoableChangeList.get(1)).isSameAs(insertion2);


        // when
        undoManager.undo();
        undoManager.undo();

        // then

        assertThat(undoManager.canRedo()).isTrue();
        assertThat(redoableChangeList).hasSize(3);
        assertThat(redoableChangeList.get(0)).isSameAs(insertion3);
        assertThat(redoableChangeList.get(1)).isSameAs(insertion2);
        assertThat(redoableChangeList.get(2)).isSameAs(insertion1);

        assertThat(undoManager.canUndo()).isFalse();
        assertThat(undoableChangeList).hasSize(0);

        // when
        undoManager.redo();
        undoManager.redo();
        undoManager.redo();

        // then

        assertThat(undoManager.canRedo()).isFalse();
        assertThat(redoableChangeList).hasSize(0);

        assertThat(undoManager.canUndo()).isTrue();
        assertThat(undoableChangeList).hasSize(3);
        assertThat(undoableChangeList.get(0)).isSameAs(insertion1);
        assertThat(undoableChangeList.get(1)).isSameAs(insertion2);
        assertThat(undoableChangeList.get(2)).isSameAs(insertion3);
    }

    @Test
    public void should_clear_list_of_redoable_changes_when_registering_new_change() {
        //given

        final Change insertion1 = new Insertion();
        final Change insertion2 = new Insertion();
        final Change insertion3 = new Insertion();

        undoManager.registerChange(insertion1);
        undoManager.registerChange(insertion2);
        undoManager.registerChange(insertion3);

        final List<Change> redoableChangeList = ((UndoManagerImpl) undoManager).getRedoableChangeList();

        // then
        undoManager.undo();

        assertThat(undoManager.canRedo()).isTrue();
        assertThat(redoableChangeList.get(0)).isSameAs(insertion3);

        // when
        final Change insertion4 = new Insertion();
        undoManager.registerChange(insertion4);

        // then
        assertThat(undoManager.canRedo()).isFalse();
        assertThat(redoableChangeList).isEmpty();
    }

    @Test
    public void should_leave_undoableChangeList_unchanged_if_undo_failed() {
        //given
        final Document documentMockThrowingExceptionWhenDeleting = Mockito.mock(Document.class);

        Mockito.doThrow(new IllegalStateException("simulated deletion error"))
                .when(documentMockThrowingExceptionWhenDeleting).delete(anyInt(), anyString());

        undoManager = new UndoManagerImpl(documentMockThrowingExceptionWhenDeleting, BUFFER_SIZE);

        final Change insertion1 = new Insertion();
        final Change insertion2 = new Insertion();
        final Change insertion3 = new Insertion();

        undoManager.registerChange(insertion1);
        undoManager.registerChange(insertion2);
        undoManager.registerChange(insertion3);

        final List<Change> undoableChangeList = ((UndoManagerImpl) undoManager).getUndoableChangeList();
        final List<Change> redoableChangeList = ((UndoManagerImpl) undoManager).getRedoableChangeList();

        // when
        try {
            undoManager.undo();
            Assert.fail("IllegalStateException should have been thrown");
        } catch (final IllegalStateException e) {
            assertThat(redoableChangeList).isEmpty();
            assertThat(undoableChangeList).hasSize(3);
        }
    }

    @Test
    public void should_leave_redoableChangeList_unchanged_if_undo_failed() {
        //given
        final Document documentMockThrowingExceptionWhenInserting = Mockito.mock(Document.class);

        Mockito.doThrow(new IllegalStateException("simulated insertion error"))
                .when(documentMockThrowingExceptionWhenInserting).insert(anyInt(), anyString());

        undoManager = new UndoManagerImpl(documentMockThrowingExceptionWhenInserting, BUFFER_SIZE);

        final Change insertion1 = new Insertion();
        final Change insertion2 = new Insertion();
        final Change insertion3 = new Insertion();

        undoManager.registerChange(insertion1);
        undoManager.registerChange(insertion2);
        undoManager.registerChange(insertion3);

        final List<Change> undoableChangeList = ((UndoManagerImpl) undoManager).getUndoableChangeList();
        final List<Change> redoableChangeList = ((UndoManagerImpl) undoManager).getRedoableChangeList();

        // when
        undoManager.undo();
        try {
            undoManager.redo();
            Assert.fail("IllegalStateException should have been thrown");
        } catch (final IllegalStateException e) {
            assertThat(redoableChangeList).hasSize(1);
            assertThat(undoableChangeList).hasSize(2);
        }
    }

    @Test
    public void should_throw_exception_when_trying_to_undo_with_empty_undoableChangeList() {
        //given

        final List<Change> undoableChangeList = ((UndoManagerImpl) undoManager).getUndoableChangeList();
        assertThat(undoableChangeList).isEmpty();

        // when
        try {
            undoManager.undo();
            Assert.fail("IllegalStateException should have been thrown");
        } catch (final IllegalStateException e) {
            assertThat(e.getMessage()).isEqualTo("No changes to undo");
        }
    }

    @Test
    public void should_throw_exception_when_trying_to_undo_with_empty_redoableChangeList() {
        //given

        final List<Change> undoableChangeList = ((UndoManagerImpl) undoManager).getRedoableChangeList();
        assertThat(undoableChangeList).isEmpty();

        // when
        try {
            undoManager.redo();
            Assert.fail("IllegalStateException should have been thrown");
        } catch (final IllegalStateException e) {
            assertThat(e.getMessage()).isEqualTo("No changes to redo");
        }
    }

}