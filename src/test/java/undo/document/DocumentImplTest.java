package undo.document;


import org.junit.Assert;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class DocumentImplTest {

    // DELETE //

    @Test
    public void should_throw_exception_when_calling_delete_with_illegal_pos_value() {
        // given
        final Document emptyDocument = new DocumentImpl();

        try {
            emptyDocument.delete(1, "content");
            Assert.fail("IllegalStateException should have been thrown");
        } catch (final IllegalStateException expected) {
            assertThat(expected.getMessage()).isEqualTo("pos [1] is beyond the length of the document [0]");
        }
    }

    @Test
    public void should_throw_exception_when_calling_delete_with_non_existing_string_at_pos() {
        // given
        final Document document = new DocumentImpl();
        document.insert(0, "content");

        try {
            document.delete(3, "aaaa");
            Assert.fail("IllegalStateException should have been thrown");
        } catch (final IllegalStateException expected) {
            assertThat(expected.getMessage()).isEqualTo("document at pos [3] doesnt contains String [aaaa], but [tent] instead");
        }
    }


    @Test
    public void should_delete_string_from_document() {
        // given
        final Document document = new DocumentImpl();
        document.insert(0, "content");

        document.delete(3, "tent");
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("con");
    }

    // INSERT //

    @Test
    public void should_throw_exception_when_calling_insert_with_illegal_pos_value() {
        // given
        final Document emptyDocument = new DocumentImpl();

        try {
            emptyDocument.insert(1, "content");
            Assert.fail("IllegalStateException should have been thrown");
        } catch (final IllegalStateException expected) {
            assertThat(expected.getMessage()).isEqualTo("pos [1] is beyond the length of the document [0]");
        }
    }

    @Test
    public void should_insert_string_in_empty_document() {
        // given
        final Document document = new DocumentImpl();

        // when
        document.insert(0, "test");

        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("test");
    }

    @Test
    public void should_insert_string_in_non_empty_document() {
        // given
        final Document document = new DocumentImpl();
        document.insert(0, "tt");

        // when
        document.insert(1, "es");

        // then
        assertThat(((DocumentImpl) document).getContent()).isEqualTo("test");
    }

    // SET_DOT //

    @Test
    public void should_throw_exception_when_calling_setDot_with_illegal_pos_value() {
        // given
        final Document emptyDocument = new DocumentImpl();

        try {
            emptyDocument.setDot(1);
            Assert.fail("IllegalStateException should have been thrown");
        } catch (final IllegalStateException expected) {
            assertThat(expected.getMessage()).isEqualTo("pos [1] is beyond the length of the document [0]");
        }
    }
}