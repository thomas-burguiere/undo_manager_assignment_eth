package undo.change;

public class ChangeFactoryImpl implements ChangeFactory {
    /**
     * Creates a deletion change.
     *
     * @param pos The position to start the deletion.
     * @param s The string to delete.
     * @param oldDot The dot (cursor) position before the deletion.
     * @param newDot The dot (cursor) position after the deletion.
     * @return The deletion {@link Change}.
     */
    @Override
    public Change createDeletion(final int pos, final String s, final int oldDot, final int newDot) {
        return new Deletion(pos, s, oldDot, newDot);
    }

    /**
     * Creates an insertion change.
     *
     * @param pos The position at which to insert.
     * @param s The string to insert.
     * @param oldDot The dot (cursor) position before the insertion.
     * @param newDot The dot (cursor) position after the insertion.
     * @return The insertion {@link Change}.
     */
    @Override
    public Change createInsertion(final int pos, final String s, final int oldDot, final int newDot) {
        return new Insertion(pos, s, oldDot, newDot);
    }
}
