package undo.change;

import undo.document.Document;

public class Insertion implements Change {
    private final int pos;
    private final String s;
    private final int oldDot;
    private final int newDot;

    public Insertion(){
        this(0, "",0,0);
    }

    public Insertion(final int pos, final String s, final int oldDot, final int newDot) {
        this.pos = pos;
        this.s = s;
        this.oldDot = oldDot;
        this.newDot = newDot;
    }

    @Override
    public String getType() {
        return "insertion";
    }

    @Override
    public void apply(final Document doc) {
        // TODO check if this is correct
        doc.insert(pos, s);
    }

    @Override
    public void revert(final Document doc) {
        // TODO check if this is correct
        doc.delete(pos, s);
    }
}
