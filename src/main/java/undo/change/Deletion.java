package undo.change;

import undo.document.Document;

public class Deletion implements Change {
    private final int pos;
    private final String s;
    private final int oldDot;
    private final int newDot;

    public Deletion(){
        this(0, "",0,0);
    }

    public Deletion(final int pos, final String s, final int oldDot, final int newDot) {

        this.pos = pos;
        this.s = s;
        this.oldDot = oldDot;
        this.newDot = newDot;
    }

    @Override
    public String getType() {
        return "deletion";
    }

    @Override
    public void apply(final Document doc) {
        // TODO check if this is correct
        doc.delete(pos, s);
    }

    @Override
    public void revert(final Document doc) {
        // TODO check if this is correct
        doc.insert(pos, s);
    }
}
