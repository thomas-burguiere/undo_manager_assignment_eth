package undo.document;

public class DocumentImpl implements Document {

    private static final String ILLEGAL_POS_EXCEPTION_MESSAGE = "pos [%d] is beyond the length of the document [%d]";

    private final StringBuilder content = new StringBuilder();
    private int dot = 0;


    @Override
    public void delete(final int pos, final String s) {
        if (content.length() < pos) {
            throw new IllegalStateException(String.format(ILLEGAL_POS_EXCEPTION_MESSAGE, pos, content.length()));
        }

        final int endPos = pos + s.length();
        final String putativeDeletedContent = content.substring(pos, endPos);
        if (!putativeDeletedContent.equals(s)) {
            throw new IllegalStateException(String.format("document at pos [%d] doesnt contains String [%s], but [%s] instead", pos, s, putativeDeletedContent));
        }

        content.delete(pos, endPos);
    }

    @Override
    public void insert(final int pos, final String s) {
        if (content.length() < pos) {
            throw new IllegalStateException(String.format(ILLEGAL_POS_EXCEPTION_MESSAGE, pos, content.length()));
        }
        content.insert(pos, s);
    }

    @Override
    public void setDot(final int pos) {
        if (content.length() < pos) {
            throw new IllegalStateException(String.format(ILLEGAL_POS_EXCEPTION_MESSAGE, pos, content.length()));
        }
        this.dot = pos;
    }

    public String getContent() {
        return content.toString();
    }

    public int getDot() {
        return dot;
    }
}
