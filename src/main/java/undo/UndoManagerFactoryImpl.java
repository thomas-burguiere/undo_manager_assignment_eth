package undo;

import undo.document.Document;

public class UndoManagerFactoryImpl implements UndoManagerFactory {
    @Override
    public UndoManager createUndoManager(final Document doc, final int bufferSize) {
        return new UndoManagerImpl(doc, bufferSize);
    }
}
