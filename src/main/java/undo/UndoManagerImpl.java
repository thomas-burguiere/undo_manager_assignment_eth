package undo;

import undo.change.Change;
import undo.document.Document;

import java.util.ArrayList;
import java.util.List;

public class UndoManagerImpl implements UndoManager {

    private final Document document;
    private final int bufferSize;

    private final List<Change> undoableChangeList;
    private final List<Change> redoableChangeList;

    public UndoManagerImpl(final Document doc, final int bufferSize) {
        document = doc;
        this.bufferSize = bufferSize;
        this.undoableChangeList = new ArrayList<>();
        this.redoableChangeList = new ArrayList<>();
    }


    @Override
    public void registerChange(final Change change) {
        redoableChangeList.clear();

        if (undoableChangeList.size() > bufferSize) {
            throw new IllegalStateException("change list size exceeding buffer size");
        }
        if (undoableChangeList.size() == bufferSize) {
            undoableChangeList.remove(0);
        }
        undoableChangeList.add(change);
    }

    @Override
    public boolean canUndo() {
        return !undoableChangeList.isEmpty();
    }

    @Override
    public boolean canRedo() {
        return !redoableChangeList.isEmpty();
    }

    @Override
    public void undo() {
        if (!canUndo()) {
            throw new IllegalStateException("No changes to undo");
        }
        final Change change = popLastUndoableChange();
        try {
            change.revert(document);
            redoableChangeList.add(change);
        } catch (final IllegalStateException e) {
            undoableChangeList.add(change);
            throw new IllegalStateException("Change undo operation failed", e);
        }
    }

    @Override
    public void redo() {
        if (!canRedo()) {
            throw new IllegalStateException("No changes to redo");
        }
        final Change change = popLastRedoableChange();
        try {
            change.apply(document);
            undoableChangeList.add(change);
        } catch (final IllegalStateException e) {
            redoableChangeList.add(change);
            throw new IllegalStateException("Change redo operation failed", e);
        }
    }

    private Change popLastUndoableChange() {
        return undoableChangeList.remove(undoableChangeList.size() - 1);
    }

    private Change popLastRedoableChange() {
        return redoableChangeList.remove(redoableChangeList.size() - 1);
    }

    List<Change> getUndoableChangeList() {
        return undoableChangeList;
    }

    List<Change> getRedoableChangeList() {
        return redoableChangeList;
    }
}
